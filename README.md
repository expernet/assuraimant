# AssurAimant
Est l'applciation de partage de AssurZero

## AssurZero
L’entreprise ASSURZERO est une entreprise spécialisée dans les assurances pour les professionnels.  L'entreprise, basé dans l’Ouest de l’île de La Réunion,  à été crée en 2015 et compte aujourd’hui une vingtaine de collaborateurs. L’entreprise ne possède pas d’agence pour l'accueil de sa clientèle, toutes les demandes et démarches se font exclusivement depuis le site web de l’entreprise accessible depuis Internet.

L’entreprise s’est doté par l'intermédiaire d’un prestataire une infrastructure informatique pour le déroulement de son activité. 

Les utilisateurs de l’entreprise accèdent au réseau par l'intermédiaire du serveur d'authentification qui assure un premier niveau de sécurité. Le pare-feu assure le filtrage des flux entrants et sortants du réseau et plus notamment les accès extérieur vers le serveur Web. Le serveur de sauvegarde, assure la résilience des données de l’application de l’entreprise en procédant à des sauvegardes régulières du serveur Web.

Très axée sur la collaboration et le partage, le responsable de l’entreprise a dédié, par l'intermédiaire de son serveur web, un espace pour ces utilisateurs afin d’y déposer des idées innovantes, qui une fois adoptées pourraient contribuer à l’évolution de l’activité.
 
Consciente des risques de sécurité pouvant mettre à mal son système d’information, l’entreprise fait appel à vos services afin de déceler les éventuelles vulnérabilités de son réseau informatique et proposer des mesures d’amélioration.


