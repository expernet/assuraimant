<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomepageController extends Controller {

	/**
	 * Controller for homepage displaying all posts
	 *
	 * @param PostRepository $postRepository
	 * @return Response
	 *
	 * @Route("/", name="connected_homepage")
	 */
	 public function index(PostRepository $postRepository) : Response {

		$posts = array();

		$posts = $postRepository->findAll();

		// don't keep duplicates
		// $posts = array_unique($posts);

		// Sort them from the most recent
		usort($posts, function (Post $a, Post $b) {
			return $a->getCreatedAt() < $b->getCreatedAt();
		});

		return $this->render('post/index.html.twig', ['mainNavHome'=>true, 'posts' => $posts]);
	 }

}
