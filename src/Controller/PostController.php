<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 */
class PostController extends Controller
{
	// /**
	//  * Accessible only for ``ADMIN`` and allow the acess to all tickets
	//  *
	//  * @param TicketRepository $ticketRepository
	//  * @return Response
	//  *
	//  * @Route("/admin/ticket/", name="ticket_index", methods="GET")
	//  */
	// public function index(TicketRepository $ticketRepository): Response
	// {
	// 	return $this->render('ticket/index.html.twig', ['tickets' => $ticketRepository->findAll()]);
	// }

	/**
	 * Controller to create a new ticket
	 *
	 * @param Request $request
	 * @return Response
	 *
	 *  @Route("/post/new", name="post_new", methods="GET|POST")
	 */
	public function new(Request $request): Response
	{

		$em = $this->getDoctrine()->getManager();
		// $em = $this->getEntityManager();
		// $repoU = $em->getRepository(User::class);
		// $users = $repoU->findAll();

		// $userChoices = array();
		// foreach($users as $u){
		//     // TODO
		// }

		// $choices = ['choices' => $users];

		// $choice_labels = function($user, $key, $value) {
		// 	/** @var User $user */
		// 	return strtoupper($user->getEmail());
		// };

		// $choices_pref = function($user, $key, $value) {
		// 	return $user->getId() == $this->getUser()->getId();
		// };

		// $choices = ['choices' => [
		// 	'choices' => $users,
		// 	'choice_label' => $choice_labels,
		// 	'preferred_choices' => $choices_pref
		// ]];

		// dump($choices); // DEBUG
		$post = new Post();
		$form = $this->createForm(PostType::class, $post);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$post->setCreatedAt(new \DateTime());
			$em = $this->getDoctrine()->getManager();
			$em->persist($post);
			$em->flush();

			return $this->redirectToRoute('connected_homepage');
		}

		return $this->render('post/new.html.twig', [
			'post' => $post,
			'form' => $form->createView(),
		]);
	}

	/**
	 * Controller to retrieve a single ticket
	 *
	 * @param Post $post
	 * @return Response
	 *
	 * @Route("/post/{id}", name="post_show", methods="GET")
	 */
	public function show(Post $post): Response
	{
		return $this->render('post/show.html.twig', ['post' => $post]);
	}

	/**
	 * Controller to edit a post
	 *
	 * @param Request $request
	 * @param Post $post
	 * @return Response
	 *
	 * @Route("/admin/post/{id}/edit", name="post_edit", methods="GET|POST")
	 */
	public function edit(Request $request, Post $post): Response
	{
		$form = $this->createForm(PostType::class, $post);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute('post_edit', ['id' => $post->getId()]);
		}

		return $this->render('post/edit.html.twig', [
			'post' => $post,
			'form' => $form->createView(),
		]);
	}

	// /**
	//  * Controller to delete a ticket
	//  *
	//  * @param Request $request
	//  * @param Ticket $ticket
	//  * @return Response
	//  *
	//  * @Route("/admin/ticket/{id}", name="ticket_delete", methods="DELETE")
	//  */
	// public function delete(Request $request, Ticket $ticket): Response
	// {
	// 	if ($this->isCsrfTokenValid('delete'.$ticket->getId(), $request->request->get('_token'))) {
	// 		$em = $this->getDoctrine()->getManager();
	// 		$em->remove($ticket);
	// 		$em->flush();
	// 	}

	// 	return $this->redirectToRoute('ticket_index');
	// }
}
