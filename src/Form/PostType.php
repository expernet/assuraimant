<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PostType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		dump($options['choices']);
		$builder
			->add('title', null, [
				'label'		=> 'Titre',
				'required'	=> true,
			])
			->add('content', null, [
				'label'		=> 'Contenu',
				'required'	=> true,
			])
			// ->add('weight', null, [
			// 	'label'		=> 'Poids',
			// 	'required'	=> true,
			// ])
			// ->add('unit', null, [
			// 	'label'		=> 'Unité',
			// 	'required'	=> true,
			// ])
			// ->add('coupeur', ChoiceType::class, $options['choices'], [
			// 	'label'		=> 'Sélection du coupeur',
			// 	'required'	=> true,
			// ])
			// ->add('chargeur', ChoiceType::class, $options['choices'], [
			// 	'label'		=> 'Sélection du chargeur',
			// 	'required'	=> true,
			// ])
			// ->add('chauffeur', ChoiceType::class, $options['choices'], [
			// 	'label'		=> 'Sélection du chauffeur',
			// 	'required'	=> true,
			// ])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Post::class,
			'choices' => null,
		]);
	}
}
